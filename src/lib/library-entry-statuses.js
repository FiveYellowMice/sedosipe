module.exports = {
  list: ['all', 'current', 'planned', 'completed', 'on_hold', 'dropped'],
  selections: ['current', 'planned', 'completed', 'on_hold', 'dropped'],
  icons: {
    all: 'group_work',
    current: 'play_circle_filled',
    planned: 'watch_later',
    completed: 'check_circle',
    on_hold: 'pause_circle_filled',
    dropped: 'cancel'
  }
};
